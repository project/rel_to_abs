CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

There's a problem with images on simplenews newsletters and feed images,
because the path to images and internal links are referrer in relation to
current site.
Also, when content is shared between different environments you may see issues
with links are referring to local files which are not synced with production.

This module implements a simple filter which replaces src and href attributes
like /path/to/link with http://www.example.com/path/to/link, making use of
core's url() function.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/rel_to_abs

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/rel_to_abs


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------



MAINTAINERS
-----------

Current maintainers:
 * Andrii Podanenko (podarok) - https://www.drupal.org/u/podarok
 * Alex Schedrov (sanchiz) - https://www.drupal.org/u/sanchiz
 * Andrii Tiupa (andrii.tiupa) - https://www.drupal.org/u/andriitiupa
 * Dima Danylevskyi (danylevskyi) - https://www.drupal.org/u/danylevskyi
 * Lourenzo Ferreira (lourenzo) - https://www.drupal.org/u/lourenzo
