<?php

namespace Drupal\rel_to_abs\Plugin\Filter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to convert relative paths to absolute URLs.
 *
 * @Filter(
 *   id = "rel_to_abs",
 *   title = @Translation("Convert relative paths to absolute URLs"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class RelToAbs extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  public LoggerInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('rel_to_abs');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $resultText = preg_replace_callback('/(href|background|src)=["\']([\/#][^"\']*)["\']/', function ($matches) {
      $url = preg_replace('/\/{2,}/', '/', $matches[2]);
      try {
        $url = Url::fromUserInput($url)->setAbsolute()->toString();
      }
      catch (\InvalidArgumentException $e) {
        $this->logger->error($e->getMessage());
      }
      return $matches[1] . '="' . $url . '"';
    }, $text);

    return new FilterProcessResult($resultText);
  }

}
